
#======================================================================================================================#
#======================================================================================================================#
#                                            Project Title - Ship Routes
#======================================================================================================================#
#======================================================================================================================#

# Load the libraries. I assume they are installed on the server.
library(bs4Dash)
library(shiny)
library(fresh)
library(leaflet)
library(dplyr)

# Load the data set ----
ships   <- as.data.frame(data.table::fread(file = "ships.csv", colClasses = c(SHIP_ID = 'character')))

#======================================================================================================================#
# Prepare the UI Theme ----
#======================================================================================================================#
bs4DashTheme <- 
  create_theme(
    bs4dash_vars(
      navbar_light_color = "#bec5cb",
      navbar_light_active_color = "#FFF",
      navbar_light_hover_color = "#FFF"
    ),
    bs4dash_yiq(
      contrasted_threshold = 10,
      text_dark = "#FFF", 
      text_light = "#272c30"
    ),
    bs4dash_layout(
      main_bg = "#353c42"
    ),
    bs4dash_sidebar_light(
      bg = "#272c30", 
      color = "#bec5cb",
      hover_color = "#FFF",
      submenu_bg = "#272c30", 
      submenu_color = "#FFF", 
      submenu_hover_color = "#FFF"
    ),
    bs4dash_status(
      primary = "#5E81AC", danger = "#BF616A", light = "#272c30"
    ),
    bs4dash_color(
      gray_900 = "#FFF"
    )
  )



#======================================================================================================================#
#                                             UI Code ----
#======================================================================================================================#

ui <- bs4DashPage(
  title = "Ship Routes",
  navbar = bs4DashNavbar(skin = "light"),
  sidebar = bs4DashSidebar(
    expand_on_hover = TRUE,
    title = "Ship Routes",
    skin = "dark",
    shinyWidgets::pickerInput(inputId = 'selected_ship_type', 
                              label = 'Ship Type', 
                              choices = unique(ships$ship_type), 
                              selected = unique(ships$ship_type)[1], 
                              multiple = FALSE, 
                              options = list(
                                `actions-box` = TRUE,
                                style = "btn-primary",
                                size = 10, 
                                `live-search` = TRUE
                              )
                              ),

    
    shinyWidgets::pickerInput(inputId = 'ship_id', 
                              label = 'Vessel ID', 
                              choices = 'placeholder', 
                              multiple = FALSE, 
                              options = list(
                                `actions-box` = TRUE,
                                style = "btn-primary",
                                size = 10, 
                                `live-search` = TRUE
                              ))
    
  ),
  
  
  body = bs4DashBody(
    use_theme(bs4DashTheme),
    bs4TabItems(
      bs4TabItem(
        tabName = "tab1",
        splitLayout(cellWidths = c('75%', '25%'),
                    shinycssloaders::withSpinner(
                      leafletOutput(outputId = 'leaflet_map', height = 560), type = 8),
                   column(width = 12,
                     bs4InfoBoxOutput("box_nobservations", width = 12),
                     bs4InfoBoxOutput("box_travelled_distance", width = 12),
                     bs4InfoBoxOutput("box_avg_speed", width = 12),
                     bs4InfoBoxOutput("box_longest_period_no_data", width = 12),
                     bs4InfoBoxOutput("box_avg_distance", width = 12),
                     bs4InfoBoxOutput("box_max_distance", width = 12)
                   )
                    ),
        
        bs4Card(
          title = "Ship Data",
          closable = FALSE,
          width = 12,
          status = "warning",
          solidHeader = FALSE,
          collapsible = TRUE,
          collapsed = TRUE,
          maximizable = TRUE,
        shinycssloaders::withSpinner(
          DT::dataTableOutput("ship_info_tbl"), type = 8)
        )
        
        
      )
    )
  )
)



#======================================================================================================================#
#                                             Server Code ----
#======================================================================================================================#
server <- function(input, output, session) {

  # Filter for ship type
  ship_types <- reactive({
    
    filter_index   <- which(ships$ship_type == input$selected_ship_type)
    return(ships[filter_index, ])
  })
  
  # Update the ship ids based on the selected ship type.
  observe({
    shinyWidgets::updatePickerInput(session = session, 
                                    inputId = 'ship_id', 
                                    choices = unique(ship_types()[['SHIP_ID']]))
    
    
    })
  
  
  #====================================================================================================================#
  #                      Find distance between two adjacent points for the selected ship ----
  #====================================================================================================================#
  
  selected_ship <- reactive({
    filtered_ships <- ship_types()
    validate(need(!is.null(ship_types()), message = 'Missing data for the selected ship. Please choose another ship id.'))
    
    selected_ship  <- 
      filtered_ships %>%
      dplyr::filter(SHIP_ID == input$ship_id) %>% 
      dplyr::mutate(DISTANCE_TRAVELLED = NA) %>% 
      #dplyr::distinct(LAT, LON, .keep_all = TRUE) %>% 
      dplyr::arrange(DATETIME)
    
    validate(need(nrow(selected_ship)>=2, message = 'At least two observations with unique spatial location are required to compute non-zero distances.'))
    
    
    return(selected_ship)
    
  })
  
  
  # Estimate the distance between two adjacent points ----
  ship_summary_reactive <- reactive({
    
    selected_ship <- selected_ship()
    
    for (i in 1:(nrow(selected_ship) - 1)) {
          selected_ship$DISTANCE_TRAVELLED[i] <- 
           as.numeric(geodist::geodist(x = selected_ship[i, c('LAT', 'LON')], 
                                       y =  selected_ship[i + 1, c('LAT', 'LON')], 
                                       measure = 'haversine'))
      
    }
    
    
    selected_ship <- 
      selected_ship %>% dplyr::filter(DISTANCE_TRAVELLED > 0) %>%
      dplyr::mutate(CUMULATIVE_DISTANCE = cumsum(DISTANCE_TRAVELLED)) %>%
      dplyr::select(DATETIME, DISTANCE_TRAVELLED, CUMULATIVE_DISTANCE, dplyr::everything()) %>%
      dplyr::mutate_if(is.numeric, round, 2) 
    
    total_distance  <- 
      selected_ship %>%
      dplyr::summarise(TOTAL_DISTANCE = sum(DISTANCE_TRAVELLED, na.rm = TRUE))
    
    longest_distance_pair <- 
      selected_ship %>% 
      dplyr::arrange(desc(DISTANCE_TRAVELLED), desc(DATETIME)) %>% 
      dplyr::slice(1)
    
    list(ship_route_info = selected_ship, 
         total_distance_travelled = total_distance, 
         longest_dist_btw_two_points = longest_distance_pair[[1]])
  })
  
  
  
  #====================================================================================================================#
  #                                   Value Boxes with Summary Statistics for the Ship ----
  #====================================================================================================================#
  
  # Total distance travelled
  output$box_travelled_distance <- renderbs4InfoBox({
 
  box_value <- ifelse(is.null(ship_summary_reactive()), NA, ship_summary_reactive()$total_distance_travelled)
  bs4InfoBox(value = paste(box_value, 'meters'),
             title = "Travelled Distance",
              status = "primary",
              icon = "route",
              width = 5)
    
  })
  

  # Average speed
  output$box_avg_speed <- renderbs4InfoBox({
    box_value <- ifelse(is.null(ship_summary_reactive()), NA, round(mean(ship_summary_reactive()$ship_route_info$SPEED, 
                                                                        na.rm = TRUE), 2))
    bs4InfoBox(value =  paste(box_value, ''),
               title = "Average Speed",
                status = "primary",
                icon = "tachometer-alt",
                width = 5)
    
  })

  
  # Number of data points
  output$box_nobservations <- renderbs4InfoBox({
    req(ship_summary_reactive())
    bs4InfoBox(value =  nrow(ship_summary_reactive()$ship_route_info),
                title = "# Observations",
                status = "primary",
                icon = "ruler-vertical",
                width = 5)
    
  })

  
  
  # Longest time withouth an observation
  output$box_longest_period_no_data <- renderbs4InfoBox({
    req(ship_summary_reactive())
    bs4InfoBox(value =  paste(round(max(as.numeric(diff(ship_summary_reactive()$ship_route_info$DATETIME), na.rm = TRUE))/60, 2), ' minutes'),
                title = "Longest Period without Data",
                status = "success",
                icon = "ship",
                width = 5)
    
  })

  
  # Avg distance between points
  output$box_avg_distance <- renderbs4InfoBox({
    req(ship_summary_reactive())
    bs4InfoBox(value =  paste(round(mean(ship_summary_reactive()$ship_route_info$DISTANCE_TRAVELLED, na.rm = TRUE), 2), 'meters'),
                title = "Avg Distance between 2 points",
                status = "success",
                icon = "route",
                width = 5)
    
  })

  
  # Max Distance between two adjacent points 
  output$box_max_distance <- renderbs4InfoBox({
    req(ship_summary_reactive())
    
    bs4InfoBox(value =  paste(max(ship_summary_reactive()$ship_route_info$DISTANCE_TRAVELLED, na.rm = TRUE), 'meters'),
                title = "Max Distance between 2 points",
                status = "success",
                icon = "route",
                width = 5)
    
  })

  
  
  
  # Table with ship route information ----
  output$ship_info_tbl <- DT::renderDataTable(server = FALSE, {
    
    DT::datatable(ship_summary_reactive()$ship_route_info, 
                  caption = paste0('Table 1. Ship ', input$ship_id),
                  rownames = FALSE,
                  class = 'cell-border stripe',
                  extensions = c("Buttons", 'Scroller'),
                  options = list(dom = 'Bfrtip', 
                                 scrollY = 420,
                                 scroller = TRUE,
                                 scrollX = TRUE,
                                 buttons = list('copy', 'print', 
                                                list(extend = 'collection',
                                                     buttons = c('csv', 'excel', 'pdf'),
                                                     text = 'Download Data')
                                                )
                                 ), 
                  escape = FALSE
                  
    )
  })
  
  
  #====================================================================================================================#
  #                                              Leaflet Map ----
  #====================================================================================================================#
  
  output$leaflet_map <- leaflet::renderLeaflet({
    
    if(!is.null(ship_summary_reactive()$ship_route_info)){
      ship_route   <- ship_summary_reactive()$ship_route_info
      furthest_pts <- which.max(ship_route$DISTANCE_TRAVELLED)
      
      ## Use C formatting for the data displayed on mouse hover
      labels <- sprintf(
        "<strong> Latitude: %s </strong><br/>
         <strong> Longitude: %s </strong><br/> 
         Datetime: %s <br/> 
         Distance Meters: %s <br/> 
         Cumulative Distance Meters: %s <br/> 
         Ship Type : %s mW </strong><br/> 
         Ship Name : %s </strong><br/> 
         Is Parked : %s  
         </strong><br/> Destination: %s </strong> ", 
        ship_route$LAT,
        ship_route$LON,
        ship_route$DATETIME,
        round(ship_route$DISTANCE_TRAVELLED, 2),
        ship_route$CUMULATIVE_DISTANCE,
        ship_route$ship_type,
        ship_route$SHIPNAME, 
        ship_route$is_parked,
        ship_route$DESTINATION) %>% 
        lapply(HTML)
      
      
      
      route_map <- 
        tryCatch({
        leaflet(ship_route) %>%
        addProviderTiles("CartoDB.DarkMatter", 
                         options = providerTileOptions(minZoom = 1, maxZoom = 14))  %>%
        addCircleMarkers(~LON, ~LAT, 
                         label = ~labels[furthest_pts:(furthest_pts + 1)], 
                         data = ship_route[(furthest_pts):(furthest_pts + 1), ],
                         color = 'red',
                         radius = 12,
                         labelOptions = labelOptions(noHide = FALSE, direction = "bottom",
                                                     style = list(
                                                       "color" = "black",
                                                       "font-family" = "serif",
                                                       "font-style" = "italic",
                                                       "box-shadow" = "3px 3px rgba(0,0,0,0.25)",
                                                       "font-size" = "14px",
                                                       "border-color" = "rgba(0,0,0,0.5)"
                                                     ))
                         ) %>%
            addPolylines(~LON, ~LAT, weight = 1,
                         label = ~labels,
                         color = '#ff9900') %>%
        addMiniMap(
          tiles = providers$Esri.WorldStreetMap,
          toggleDisplay = TRUE)
          },
        error = function(e){'ERROR'})
      
      # Validate that the map has been created properly.
      validate(need(route_map != 'ERROR', message = 'Error raised while creating the map object. Please select another ship id.'))
      
      # Create a data set with the unique spatial positions of the ship. 
      unique_ship_positions <- ship_route %>% dplyr::distinct(LAT, LON, .keep_all = TRUE)
      n_unique_positions    <- nrow(unique_ship_positions)
      
      ## Use C formatting for the data displayed on mouse hover
      labels_unique <- sprintf(
        "<strong> Latitude: %s </strong><br/>
         <strong> Longitude: %s </strong><br/> 
         Datetime: %s <br/> 
         Distance Meters: %s <br/> 
         Cumulative Distance Meters: %s <br/> 
         Ship Type : %s mW </strong><br/> 
         Ship Name : %s </strong><br/> 
         Is Parked : %s  
         </strong><br/> Destination: %s </strong> ", 
        unique_ship_positions$LAT,
        unique_ship_positions$LON,
        unique_ship_positions$DATETIME,
        round(unique_ship_positions$DISTANCE_TRAVELLED, 2),
        unique_ship_positions$CUMULATIVE_DISTANCE,
        unique_ship_positions$ship_type,
        unique_ship_positions$SHIPNAME, 
        unique_ship_positions$is_parked,
        unique_ship_positions$DESTINATION) %>% 
        lapply(HTML)
      
      # If we have less than 85 positions, display all markers on the map, else sample them to avoid
      # imposing computational burden when building the leaflet map.
      if(n_unique_positions < 85){
        route_map  <- route_map %>%
                      addMarkers(~LON, ~LAT, 
                                 label = ~labels_unique, 
                                 data = unique_ship_positions,
                                 labelOptions = labelOptions(noHide = FALSE, direction = "bottom",
                                                             style = list(
                                                               "color" = "black",
                                                               "font-family" = "serif",
                                                               "font-style" = "italic",
                                                               "box-shadow" = "3px 3px rgba(0,0,0,0.25)",
                                                               "font-size" = "14px",
                                                               "border-color" = "rgba(0,0,0,0.5)"
                                                             )))
      } else{
        route_map  <- 
          route_map %>% 
          addMarkers(~LON, ~LAT, label = ~labels_unique, 
                     data = unique_ship_positions[c(1, sort(sample(x = 2:(nrow(unique_ship_positions) - 1),
                                                                   size = 83,
                                                                   replace = FALSE)), 
                                                    nrow(unique_ship_positions)), ],
                     labelOptions = labelOptions(noHide = FALSE, direction = "bottom",
                                                 style = list(
                                                   "color" = "black",
                                                   "font-family" = "serif",
                                                   "font-style" = "italic",
                                                   "box-shadow" = "3px 3px rgba(0,0,0,0.25)",
                                                   "font-size" = "14px",
                                                   "border-color" = "rgba(0,0,0,0.5)"
                                                 )))
        
      }
    
      return(route_map)
      
    }
    
    
   
  })
  
}


#====================================================================================================================#
#                                              Run the application ----
#====================================================================================================================#
shinyApp(ui, server)
